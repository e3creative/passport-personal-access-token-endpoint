# passport-personal-access-token-endpoint

This package provides an endpoint which acceps user credentials and returns the corresponding user (if valid) with an access token they can use to pass through the 'auth:api' middleware.

## Installation

* `composer require e3creative/passport-personal-access-token-endpoint`
* `php artisan vendor:publish`


## Routes.

This package adds one routes to your application:

* POST /api/v1/login
    Middleware: api
    Required headers:
        Authorization: Bearer [your client token]
        Accept: application/json
        Content-type: application/json
    Required data:
        email/username: the email or username of the user who wants to log in
        password: the password of the user who wants to log in
    Returns: the logged in user with an access_token which can be used to get through the 'auth:api' middleware

## Config

The following configuration will be available to you after publishing the vendor
files:

route_middleware_name:
    Description: the middleware that should be applied to your POST endpoint to
        ensure that only authorized users are able to retrieve a token
    Default: 'api'

route_prefix:
    Description: the route prefix for your POST endpoint
    Default: 'api/v1'
