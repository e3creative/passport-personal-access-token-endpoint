<?php

$parameters = [
    'middleware' => config('passport-personal-access-token-endpoint.route_middleware_name'),
    'prefix' => config('passport-personal-access-token-endpoint.route_prefix'),
];

$router = Route::group(array_filter($parameters), function ($router) {
    $router->post(
        'login',
        'E3Creative\PassportPersonalAccessTokenEndpoint\Controllers\PassportPersonalAccessTokenController@store'
    );
});
