# Changelog

## [3.0.0] - 2019-10-31
### Changed
- Updates vendor name

# [2.0.1] - 2018-11-09
## Fixes
    - returns 401 status from unsuccuessful login, not just code in JSON

# [2.0.0] - 2018-10-27
## Updates
    - endpoint response to only return access token

# [1.1.0] - 2018-10-16
## Adds
    - testbench and automated tests

# [1.0.0] - 2018-10-05
## Initial release
