<?php

namespace E3Creative\PassportPersonalAccessTokenEndpoint\Tests\Unit;

use Mockery;
use StdClass;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use E3Creative\PassportPersonalAccessTokenEndpoint\Tests\TestCase;
use E3Creative\PassportPersonalAccessTokenEndpoint\Controllers\PassportPersonalAccessTokenController;

class PassportPersonalAccessTokenControllerTest extends TestCase
{
    public function testItCannotGetPersonalAccessTokenWithInvalidLoginDetails()
    {
        $requestMock = $this->getRequestMock();

        $authMock = Mockery::mock(AuthManager::class)
            ->shouldReceive('attempt')
            ->once()
            ->andReturn(false);

        $controller = new PassportPersonalAccessTokenController($authMock->getMock());

        $response = $controller->store($requestMock->getMock())->getContent();

        $this->assertContains('Your login details are incorrect.', $response);
    }

    public function testItCanGetUserWithAccessToken()
    {
        $requestMock = $this->getRequestMock();

        $authMock = Mockery::mock(AuthManager::class)
            ->shouldReceive('attempt')
            ->once()
            ->andReturn(true)
            ->shouldReceive('user')
            ->once()
            ->andReturn($this->getUserMock());

        $controller = new PassportPersonalAccessTokenController($authMock->getMock());

        $response = $controller->store($requestMock->getMock())->getContent();

        $this->assertContains(
            json_encode(['access_token' => 'abcdefg123456']),
            $response
        );
    }

    private function getRequestMock()
    {
        return Mockery::mock(Request::class)
            ->shouldReceive('all')
            ->once()
            ->andReturn([
                'email' => 'test@test.com',
                'password' => 'secret',
            ])
            ->shouldReceive('only')
            ->once()
            ->andReturn([
                'email' => 'test@test.com',
                'password' => 'secret',
            ]);
    }

    private function getUserMock()
    {
        $token = new StdClass;
        $token->accessToken = 'abcdefg123456';

        $mock = Mockery::mock(StdClass::class)
            ->shouldReceive('createToken')
            ->once()
            ->andReturn($token);

        return $mock->getMock();
    }
}
