<?php

namespace E3Creative\PassportPersonalAccessTokenEndpoint\Tests;

use Illuminate\Hashing\HashManager;
use Illuminate\Contracts\Hashing\Hasher;
use Orchestra\Testbench\Traits\CreatesApplication;
use E3Creative\PassportPersonalAccessTokenEndpoint\Providers\ServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }
}
