<?php

namespace E3Creative\PassportPersonalAccessTokenEndpoint\Tests\Api;

use E3Creative\PassportPersonalAccessTokenEndpoint\Tests\TestCase;

class PassportPersonalAccessTokenControllerTest extends TestCase
{
    public function testItCanMakeRequestToEndpoint()
    {
        $response = $this->json('POST', '/login')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required when username is not present.'],
                    'username' => ['The username field is required when email is not present.'],
                    'password' => ['The password field is required.'],
                ],
            ]);
    }
}
