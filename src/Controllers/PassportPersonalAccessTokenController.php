<?php

namespace E3Creative\PassportPersonalAccessTokenEndpoint\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;

class PassportPersonalAccessTokenController extends Controller
{
    use ValidatesRequests;
    /**
     * @var AuthManager
     */
    public $auth;

    /**
     * @param AuthManager $auth
     * @param Router      $router
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Finds a valid token, logs in the corresponding user, invalidates the
     * token for future use and redirects to the intended URL. If no valid token
     * can be found, redirect them to the login page
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required_without:username|string',
            'username' => 'required_without:email|string',
            'password' => 'required|string',
        ]);

        $valid = $this->auth->attempt($request->only('email', 'username', 'password'));

        if (!$valid) {
            return response()->json([
                'message' => 'Your login details are incorrect.',
                'code' => 401,
            ], 401);
        }

        return response()->json([
            'access_token' => $this->auth->user()->createToken(config('app.name'))->accessToken,
        ]);
    }
}
